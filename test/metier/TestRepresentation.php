<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Test representation</title>
    </head>
    <body>
        <?php
        use modele\metier\Representation;
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier représentation</h2>";
        $objet = new Representation("l001", "la joliverie", "St Sebastient", "01/07/2020", "21", "23");
        var_dump($objet);
        ?>
    </body>
</html>