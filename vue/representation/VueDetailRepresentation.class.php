<?php

namespace vue\representation;

use vue\VueGenerique;
use modele\metier\Representation;

/**
 * Description Page de consultation d'une representation donné
 */
class VueDetailRepresentation extends VueGenerique {

    /** @var Representation identificateur de la representation à afficher */
    private $uneRepresentation;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();

        ?>
        <br>
        <table width='60%' cellspacing='0' cellpadding='0' class='tabNonQuadrille'> 
            <tr class='ligneTabNonQuad'>
                <td  width='20%'> Id : </td>
                <td><?= $this->uneRepresentation->getId() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Groupe : </td>
                <td><?= $this->uneRepresentation->getGroupe() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Lieu : </td>
                <td><?= $this->uneRepresentation->getLieu() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Date : </td>
                <td><?= $this->uneRepresentation->getDate() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Heure de début : </td>
                <td><?= $this->uneRepresentation->getHeureDebut() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Heure de fin : </td>
                <td><?= $this->uneRepresentation->getHeureFin() ?></td>
            </tr>
        </table>
        <br>
        <a href='index.php?controleur=representation&action=liste'>Retour</a>
        <?php
        include $this->getPied();
    }

    function setUneRepresentation(Representation $uneRepresentation) {
        $this->uneRepresentation = $uneRepresentation;
    }


}
