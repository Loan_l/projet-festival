<?php
/**
 * Description Page de consultation de la liste des representation
 * -> affiche Uun tableau constitué d'une ligne d'entête et d'une ligne par representation
 */
namespace vue\representation;

use vue\VueGenerique;
use modele\metier\Representation;

class VueListeRepresentations extends VueGenerique {
    
    /** @var array iliste des établissements à afficher avec leur nombre d'atttributions */
    private $lesRepresentationsAvecNbAttributions;
    

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <br>
        <table width="55%" cellspacing="0" cellpadding="0" class="tabNonQuadrille" >

            <tr class="enTeteTabNonQuad" >
                <td colspan="4" ><strong>Representation</strong></td>
            </tr>
            <?php
            // Pour chaque representation lu dans la base de données
            foreach ($this->lesRepresentationsAvecNbAttributions as $uneRepresentationAvecNbAttrib) {
                $uneRepresentation = $uneRepresentationAvecNbAttrib["rpt"];
                $id = $uneRepresentation->getId();
                ?>
                <tr class="ligneTabNonQuad" >
                    <td width="52%" ><?= $id ?></td>

                    <td width="16%" align="center" > 
                        <a href="index.php?controleur=representation&action=detail&id=<?= $id ?>" >
                            Voir détail</a>
                    </td>

                    <td width="16%" align="center" > 
                        <a href="index.php?controleur=representation&action=modifier&id=<?= $id ?>" >
                            Modifier
                        </a>
                    </td>
                    
                    <td width="16%" align="center" > 
                        <a href="index.php?controleur=representation&action=supprimer&id=<?= $id ?>" >
                            Supprimer
                        </a>
                    </td>

                    <?php
                    // S'il existe déjà des attributions pour la representation, il faudra
                    // d'abord les supprimer avant de pouvoir supprimer la representation
                    if ($uneRepresentationAvecNbAttrib["rpt"] == "" ) {
                        ?>
                        <td width="16%" align="center" > 
                            <a href="index.php?controleur=representation&action=supprimer&id=<?= $id ?>" >
                               Supprimer
                            </a>
                        </td>
                        <?php
                    } else {
                        ?>
                        <td width="16%" >&nbsp; </td>
                        <?php
                    }
                    ?>
                </tr>
                <?php
            }
            ?>
        </table>
        <br>
        <a href="index.php?controleur=representation&action=creer" >
            Création d'une representation</a >
        <?php
        include $this->getPied();
    }

    function setLesRepresentationsAvecNbAttributions($uneRepresentationAvecNbAttributions) {
        $this->lesRepresentationsAvecNbAttributions = $uneRepresentationAvecNbAttributions;
    }

}