<?php
namespace modele\metier;

class Lieu{
    private $id;
    
    private $nomLieu;
            
    private $adresse;
    
    private $capacite;
    
    function __construct($id, $nomLieu, $adresse, $capacite) {
        $this->id = $id;
        $this->nomLieu = $nomLieu;
        $this->adresse = $adresse;
        $this->capacite = $capacite;
    }
    
    function getIdLieu() {
        return $this->id;
    }
    
    function getNomLieu(){
        return $this->nomLieu;
    }
    
    function getAdresse(){
        return $this->adresse;
    }
    
    function getCapacite(){
        return $this->capacite;
    }
    
    function setId($id) {
        $this->id = $id;
    }
    
    function setNom($nomLieu){
        $this-> nomLieu = $nomLieu;
    }
    
    function setAdresse($adresse){
        $this-> adresse = $adresse;
    }
    
    function setCapacite($capacite){
        $this-> capacite = $capacite;
    }
}