<?php
namespace modele\metier;

class Representation{
    private $id;
    
    private $groupe;
    
    private $lieu;
            
    private $date;
    
    private $heureDebut;
    
    private $heureFin;
    
    function __construct($id, $groupe, $lieu, $date, $heureDebut, $heureFin) {
        $this->id = $id;
        $this->groupe = $groupe;
        $this->lieu = $lieu;
        $this->date = $date;
        $this->heureDebut = $heureDebut;
        $this->heureFin = $heureFin;
    }
    
    function getId() {
        return $this->id;
    }

    function getGroupe(){
        return $this->groupe;
    }
    function getLieu() {
        return $this->lieu;
    }

    function getDate() {
        return $this->date;
    }

    function getHeureDebut() {
        return $this->heureDebut;
    }

    function getHeureFin() {
        return $this->heureFin;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setGroupe($groupe){
        $this->groupe = $groupe;
    }
    function setLieu($lieu) {
        $this->lieu = $lieu;
    }

    function setDate($date) {
        $this->date = $date;
    }

    function setHeureDebut($heureDebut) {
        $this->heureDebut = $heureDebut;
    }

    function setHeureFin($heureFin) {
        $this->heureFin = $heureFin;
    }


}