<?php

namespace modele\dao;

use modele\metier\Lieu;
use PDOStatement;
use PDO;


class LieuDAO{
    
    protected static function enregVersMetier(array $enreg) {
        $id = $enreg['IDLIEU'];
        $nom = $enreg['NOM'];
        $adresse = $enreg['ADRESSE'];
        $capAccueil = $enreg['CAPACITE'];
        
        $unLieu = new Lieu($id, $nom, $adresse, $capAccueil);

        return $unLieu;
        
    }
      protected static function metierVersEnreg(Lieu $objetMetier, PDOStatement $stmt) {
        // On utilise bindValue plutôt que bindParam pour éviter des variables intermédiaires
        // Note : bindParam requiert une référence de variable en paramètre n°2 ; 
        // avec bindParam, la valeur affectée à la requête évoluerait avec celle de la variable sans
        // qu'il soit besoin de refaire un appel explicite à bindParam
        $stmt->bindValue(':id', $objetMetier->getIdLieu());
        $stmt->bindValue(':nom', $objetMetier->getNomLieu());
        $stmt->bindValue(':rue', $objetMetier->getAdresse());
        $stmt->bindValue(':capaciteAccueil', $objetMetier->getCapacite());
       
    }
     public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Lieu";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Pour chaque enregisterement
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                // instancier un Etablissement et l'ajouter au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
     public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Lieu WHERE idLieu = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
     public static function insert(Lieu $objet) {
        $requete = "INSERT INTO Lieu VALUES (:id, :nom, :rue, :capaciteAccueil)";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0); 
    }
    public static function update($id, Lieu $objet) {
        $ok = false;
        $requete = "UPDATE Lieu SET nom=:nom,
           capacite=:capaciteAccueil, adresse=:rue
           WHERE idLieu=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }
     public static function delete($id) {
        $ok = false;
        $requete = "DELETE FROM Lieu WHERE IDLieu = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }
}


