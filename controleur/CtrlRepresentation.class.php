<?php

/**
 * Contrôleur de gestion des établissements
 * @version 2018
 */

namespace controleur;

use controleur\GestionErreurs;
use modele\dao\GroupeDAO;
use modele\dao\RepresentationDAO;
use modele\metier\Representation;
use modele\dao\Bdd;
use vue\representation\VueListeRepresentations;
use vue\representation\VueSaisieRepresentation;
use vue\representation\VueDetailRepresentation;
use vue\representation\VueSupprimerRepresentation;

class CtrlRepresentation extends ControleurGenerique {

    /** controleur= representation & action= defaut
     * Afficher la liste des representations      */
    public function defaut() {
        $this->liste();
    }

    /** controleur= representation & action= liste
     * Afficher la liste des representation      */
    public function liste() {
        $laVue = new VueListeRepresentations();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des representation avec, pour chacun,
        Bdd::connecter();
        $laVue->setLesRepresentationsAvecNbAttributions($this->getTabRepresentationAvecNbAttributions());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Représentation");
        $this->vue->afficher();
    }

    /** controleur= representation & action=detail & id=identifiant_representation
     * Afficher une representation d'après son identifiant     */
    public function detail() {
        $idRpt = $_GET["id"];
        $this->vue = new VueDetailRepresentation();
        // Lire dans la BDD les données de la representation à afficher
        Bdd::connecter();
        $this->vue->setUneRepresentation(RepresentationDAO::getOneById($idRpt));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representation");
        $this->vue->afficher();
    }

    /** controleur= representation & action=creer
     * Afficher le formulaire d'ajout d'une representation     */
    public function creer() {
        $laVue = new VueSaisieRepresentation();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouvelle Representation");
        // En création, on affiche un formulaire vide
        /* @var Representation $unRpt */
        $uneRpt = new Representation("", "", "", "", "", "");
        $laVue->setUneRepresentation($uneRpt);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representation");
        $this->vue->afficher();
    }

    /** controleur= representation & action=validerCreer
     * ajouter d'une representation dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        /* @var Representation $uneRpt  : récupération du contenu du formulaire et instanciation d'une Representation */
        $uneRpt = new Representation($_REQUEST['id'], $_REQUEST['groupe'], $_REQUEST['lieu'], $_REQUEST['date'], $_REQUEST['heureDebut'], $_REQUEST['heureFin']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesRpt($uneRpt, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer la representation
            RepresentationDAO::insert($uneRpt);
            // revenir à la liste des representation
            header("Location: index.php?controleur=representation&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieRepresentation();
            $this->vue = $laVue;
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Nouvelle Representation");
            $laVue->setUneRepresentation($uneRpt);
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - representation");
            $this->vue->afficher();
        }
    }

    /** controleur= representation & action=modifier $ id=identifiant de la representation à modifier
     * Afficher le formulaire de modification d'une representation     */
    public function modifier() {
        $idRpt = $_GET["id"];
        $laVue = new VueSaisieRepresentation();
        $this->vue = $laVue;
        // Lire dans la BDD les données de la representation à modifier
        Bdd::connecter();
        /* @var Representation $laRepresentation */
        $laRepresentation = RepresentationDAO::getOneById($idRpt);
        $this->vue->setUneRepresentation($laRepresentation);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier la representation : " . $laRepresentation->getGroupe() . " (" . $laRepresentation->getId() . ")");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->afficher();
    }

    /** controleur= representation & action=validerModifier
     * modifier une representation dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        /* @var Representation $uneRpt  : récupération du contenu du formulaire et instanciation d'une representation */
        $uneRpt = new Representation($_REQUEST['id'], $_REQUEST['groupe'], $_REQUEST['lieu'], $_REQUEST['date'], $_REQUEST['heureDebut'], $_REQUEST['heureFin']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesRpt($uneRpt, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour la representation
            RepresentationDAO::update($uneRpt->getId(), $uneRpt);
            // revenir à la liste des representations
            header("Location: index.php?controleur=representation&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieRepresentation();
            $this->vue = $laVue;
            $laVue->setUneRepresentation($uneRpt);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier la representation : " . $uneRpt->getUnGroupe() . " (" . $uneRpt->getIdRepresentation() . ")");
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - representation");
            $this->vue->afficher();
        }
    }
    
   
    /** controleur= representation & action=supprimer & id=identifiant_representation
     * Supprimer une representation d'après son identifiant     */
    public function supprimer() {
        $idRpt = $_GET["id"];
        $this->vue = new VueSupprimerRepresentation();
        // Lire dans la BDD les données de la representation à supprimer
        Bdd::connecter();
        $this->vue->setUneRepresentation(RepresentationDAO::getOneById($idRpt));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representation");
        $this->vue->afficher();
    }

    /** controleur= representation & action= validerSupprimer
     * supprimer une representation dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant de la representation à supprimer");
        } else {
            // suppression de la representation d'après son identifiant
            RepresentationDAO::delete($_GET["id"]);
        }
        // retour à la liste des representation
        header("Location: index.php?controleur=representation&action=liste");
    }

    /**
     * Vérification des données du formulaire de saisie
     * @param Representation $uneRpt à vérifier
     * @param bool $creation : =true si formulaire de création d'une nouvelle representation ; =false sinon
     */
    private function verifierDonneesRpt(Representation $uneRpt, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if (($creation && $uneRpt->getId() == "") || $uneRpt->getGroupe() == "" ||
                $uneRpt->getDate() == "" || $uneRpt->getGroupe() == "" || $uneRpt->getheureDebut() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère* est obligatoire');
        }
          if (!preg_match("/[a-zA-Z]/", $uneRpt->getGroupe())) {
            GestionErreurs::ajouter("Un caractère du groupe n'est pas correcte");
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $uneRpt->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estAlphaNumerique($uneRpt->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (RepresentationDAO::isAnExistingId($uneRpt->getId())) {
                    GestionErreurs::ajouter("La representation " . $uneRpt->getId() . " existe déjà");
                }
            }
        }
        // Vérification qu'une representation de même id existe pas déjà (id + nom si création)
        if ($uneRpt->getId() != "" && RepresentationDAO::isAnExistingId($creation, $uneRpt->getId(), $uneRpt->getId())) {
            GestionErreurs::ajouter("La representation " . $uneRpt>getIdRepresentation() . " existe déjà");
        }
    }

    /*****************************************************************************
     * Méthodes permettant de préparer les informations à destination des vues
     ******************************************************************************/

    /**
     * Retourne la liste de toutes les representations et du nombre d'attributions de chacune
     * @return Array tableau associatif à 2 dimensions : 
     *      - dimension 1, l'index est l'id de la representation
     *      - dimension 2, index "rpt" => objet de type representation
     */
    public function getTabRepresentationAvecNbAttributions(): Array {
        $lesRepresentationsAvecNbAttrib = Array();
        $lesRepresentations = RepresentationDAO::getAll();
        foreach ($lesRepresentations as $uneRpt) {
            /* @var Groupe $unGrp */
            $lesRepresentationsAvecNbAttrib[$uneRpt->getId()]['rpt'] = $uneRpt;
        }
        return $lesRepresentationsAvecNbAttrib;
    }

}