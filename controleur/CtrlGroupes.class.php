<?php

/**
 * Contrôleur de gestion des établissements
 * @version 2018
 */

namespace controleur;

use controleur\GestionErreurs;
use modele\dao\GroupeDAO;
use modele\dao\AttributionDAO;
use modele\metier\Groupe;
use modele\dao\Bdd;
use vue\groupes\VueListeGroupes;
use vue\groupes\VueDetailGroupe;
use vue\groupes\VueSaisieGroupe;
use vue\groupes\VueSupprimerGroupe;

class CtrlGroupes extends ControleurGenerique {

    /** controleur= groupes & action= defaut
     * Afficher la liste des groupes      */
    public function defaut() {
        $this->liste();
    }

    /** controleur= groupes & action= liste
     * Afficher la liste des établissements      */
    public function liste() {
        $laVue = new VueListeGroupes();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des groupes avec, pour chacun,
        Bdd::connecter();
        $laVue->setLesGroupesAvecNbAttributions($this->getTabGroupesAvecNbAttributions());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }

    /** controleur= groupes & action=detail & id=identifiant_groupe
     * Afficher un groupe d'après son identifiant     */
    public function detail() {
        $idGrp = $_GET["id"];
        $this->vue = new VueDetailGroupe();
        // Lire dans la BDD les données du groupe à afficher
        Bdd::connecter();
        $this->vue->setUnGroupe(GroupeDAO::getOneById($idGrp));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }

    /** controleur= groupes & action=creer
     * Afficher le formulaire d'ajout d'un établissement     */
    public function creer() {
        $laVue = new VueSaisieGroupe();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouveau groupe");
        // En création, on affiche un formulaire vide
        /* @var Groupe $unGrp */
        $unGrp = new Groupe("", "", "", "", "", "", "");
        $laVue->setUnGroupe($unGrp);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }

    /** controleur= groupes & action=validerCreer
     * ajouter d'un groupe dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        /* @var Groupe $unGrp  : récupération du contenu du formulaire et instanciation d'un groupe */
        $unGrp = new Groupe($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['identiteResponsable'], $_REQUEST['adressePostale'], $_REQUEST['nombrePersonnes'], $_REQUEST['nomPays'], $_REQUEST['hebergement']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesGrp($unGrp, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer le groupe
            GroupeDAO::insert($unGrp);
            // revenir à la liste des groupe
            header("Location: index.php?controleur=groupes&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieGroupe();
            $this->vue = $laVue;
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Nouveau groupe");
            $laVue->setUnGroupe($unGrp);
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - groupes");
            $this->vue->afficher();
        }
    }

    /** controleur= groupes & action=modifier $ id=identifiant du groupe à modifier
     * Afficher le formulaire de modification d'un groupe     */
    public function modifier() {
        $idGrp = $_GET["id"];
        $laVue = new VueSaisieGroupe();
        $this->vue = $laVue;
        // Lire dans la BDD les données du groupe à modifier
        Bdd::connecter();
        /* @var Groupe $leGroupe */
        $leGroupe = GroupeDAO::getOneById($idGrp);
        $this->vue->setUnGroupe($leGroupe);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier le groupe : " . $leGroupe->getNom() . " (" . $leGroupe->getId() . ")");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }

    /** controleur= groupes & action=validerModifier
     * modifier un groupe dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        /* @var Groupe $unGrp  : récupération du contenu du formulaire et instanciation d'un groupe */
        $unGrp = new Groupe($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['identiteResponsable'], $_REQUEST['adressePostale'], $_REQUEST['nombrePersonnes'], $_REQUEST['nomPays'], $_REQUEST['hebergement']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesGrp($unGrp, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour le groupe
            GroupeDAO::update($unGrp->getId(), $unGrp);
            // revenir à la liste des groupes
            header("Location: index.php?controleur=groupes&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieGroupe();
            $this->vue = $laVue;
            $laVue->setUnGroupe($unGrp);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier le groupe : " . $unGrp->getNom() . " (" . $unGrp->getId() . ")");
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - groupes");
            $this->vue->afficher();
        }
    }
    
   
    /** controleur= groupes & action=supprimer & id=identifiant_groupe
     * Supprimer un groupes d'après son identifiant     */
    public function supprimer() {
        $idGrp = $_GET["id"];
        $this->vue = new VueSupprimerGroupe();
        // Lire dans la BDD les données du groupe à supprimer
        Bdd::connecter();
        $this->vue->setUnGroupe(GroupeDAO::getOneById($idGrp));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }

    /** controleur= groupes & action= validerSupprimer
     * supprimer un groupes dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant du groupe à supprimer");
        } else {
            // suppression du groupe d'après son identifiant
            GroupeDAO::delete($_GET["id"]);
        }
        // retour à la liste des établissements
        header("Location: index.php?controleur=groupes&action=liste");
    }

    /**
     * Vérification des données du formulaire de saisie
     * @param Groupe $unGrp groupe à vérifier
     * @param bool $creation : =true si formulaire de création d'un nouveau groupe ; =false sinon
     */
    private function verifierDonneesGrp(Groupe $unGrp, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if (($creation && $unGrp->getId() == "") || $unGrp->getNom() == "" ||
                $unGrp->getNbPers() == "" || $unGrp->getNom() == "" || $unGrp->getHebergement() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère* est obligatoire');
        }
          if (!preg_match("/[a-zA-Z]/", $unGrp->getNom())) {
            GestionErreurs::ajouter("Un caractère du nom n'est pas correcte");
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $unGrp->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estAlphaNumerique($unGrp->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (GroupeDAO::isAnExistingId($unGrp->getId())) {
                    GestionErreurs::ajouter("Le groupe " . $unGrp->getId() . " existe déjà");
                }
            }
        }
        // Vérification qu'un groupe de même nom n'existe pas déjà (id + nom si création)
        if ($unGrp->getNom() != "" && GroupeDAO::isAnExistingName($creation, $unGrp->getId(), $unGrp->getNom())) {
            GestionErreurs::ajouter("Le groupe " . $unGrp->getNom() . " existe déjà");
        }
    }

    /*****************************************************************************
     * Méthodes permettant de préparer les informations à destination des vues
     ******************************************************************************/

    /**
     * Retourne la liste de tous les groupes et du nombre d'attributions de chacun
     * @return Array tableau associatif à 2 dimensions : 
     *      - dimension 1, l'index est l'id du groupe
     *      - dimension 2, index "grp" => objet de type groupe
     */
    public function getTabGroupesAvecNbAttributions(): Array {
        $lesGroupesAvecNbAttrib = Array();
        $lesGroupes = GroupeDAO::getAll();
        foreach ($lesGroupes as $unGrp) {
            /* @var Groupe $unGrp */
            $lesGroupesAvecNbAttrib[$unGrp->getId()]['grp'] = $unGrp;
        }
        return $lesGroupesAvecNbAttrib;
    }

}
